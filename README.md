# TheOtherUnknown.gitlab.io

[![build status](https://gitlab.com/TheOtherUnknown/theotherunknown.gitlab.io/badges/master/build.svg)](https://gitlab.com/TheOtherUnknown/theotherunknown.gitlab.io/commits/master)

My website. I have no idea *why* you're reading this, but I seriously doubt bad HTML and confusing CSS will help you with anything. 


Look at WC3 or something.